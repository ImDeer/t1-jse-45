package t1.dkhrunina.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public class TaskCompleteByIndexResponse extends AbstractResultResponse {

    @NotNull
    private TaskDTO task;

    public TaskCompleteByIndexResponse(@NotNull final TaskDTO task) {
        this.task = task;
    }

    public TaskCompleteByIndexResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}