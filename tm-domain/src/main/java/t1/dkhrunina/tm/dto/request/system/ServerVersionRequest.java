package t1.dkhrunina.tm.dto.request.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import t1.dkhrunina.tm.dto.request.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
public class ServerVersionRequest extends AbstractRequest {
}