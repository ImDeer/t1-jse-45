package t1.dkhrunina.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public class ProjectChangeStatusByIndexResponse extends AbstractResultResponse {

    @Nullable
    private ProjectDTO project;

    public ProjectChangeStatusByIndexResponse(@Nullable final ProjectDTO project) {
        this.project = project;
    }

    public ProjectChangeStatusByIndexResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}