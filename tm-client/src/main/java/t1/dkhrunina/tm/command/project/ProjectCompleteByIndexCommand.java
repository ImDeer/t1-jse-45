package t1.dkhrunina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.project.ProjectCompleteByIndexRequest;
import t1.dkhrunina.tm.dto.response.project.ProjectCompleteByIndexResponse;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "pr-complete-by-index";

    @NotNull
    private static final String DESCRIPTION = "Complete project by index.";

    @Override
    public void execute() {
        System.out.println("[Complete project by index]");
        System.out.println("Enter index: ");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(getToken(), index);
        @NotNull final ProjectCompleteByIndexResponse response = getProjectEndpoint().completeProjectByIndex(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}