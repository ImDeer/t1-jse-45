package t1.dkhrunina.tm.api.repository.model;

import t1.dkhrunina.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}