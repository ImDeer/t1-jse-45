package t1.dkhrunina.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User add(@NotNull User user);

    void clear();

    boolean existsById(@Nullable String id);

    List<User> findAll();

    User findOneById(@Nullable String id);

    User findOneByIndex(@Nullable Integer index);

    int getSize();

    @NotNull
    User remove(@Nullable User user);

    User removeById(@Nullable String id);

    User removeByIndex(@Nullable Integer index);

    void update(@NotNull User user);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email, @Nullable Role role);

    User findByLogin(@Nullable String login);

    User findByEmail(@Nullable String email);

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    User removeByEmail(@Nullable String email);

    Collection<User> set(@NotNull Collection<User> users);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName,
                    @Nullable String middleName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}