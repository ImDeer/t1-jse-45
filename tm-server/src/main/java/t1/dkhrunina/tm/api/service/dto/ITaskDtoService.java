package t1.dkhrunina.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDTO> {

    @NotNull
    TaskDTO changeTaskStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    TaskDTO changeTaskStatusByIndex(@NotNull String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    TaskDTO create(@NotNull String userId, @Nullable String name);

    @NotNull
    TaskDTO create(@NotNull String userId, @Nullable String name, @Nullable String description);

    void clear();

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @Nullable String projectId);

    @Nullable
    List<TaskDTO> findAll();

    @Nullable
    @SuppressWarnings("rawtypes")
    List<TaskDTO> findAll(@NotNull String userId, @Nullable Comparator comparator);

    @Nullable
    List<TaskDTO> findAll(@NotNull String userId, @Nullable Sort sort);

    void removeAllByProjectId(@NotNull String userId, @Nullable String projectId);

    @NotNull Collection<TaskDTO> set(@NotNull Collection<TaskDTO> tasks);

    void update(@NotNull TaskDTO task);

    @NotNull
    TaskDTO updateById(@NotNull String userId, @Nullable String id, @Nullable String name,
                       @Nullable String description);

    @NotNull
    TaskDTO updateByIndex(@NotNull String userId, @Nullable Integer index, @Nullable String name,
                          @Nullable String description);

}