package t1.dkhrunina.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.TaskDTO;

public interface IProjectTaskDtoService {

    @NotNull
    TaskDTO bindTaskToProject(@NotNull String userId, @Nullable String projectId,
                              @Nullable String taskId);

    void removeAllByUserId(@NotNull String userId);

    void removeProjectById(@NotNull String userId, @Nullable String projectId);

    void removeProjectByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    TaskDTO unbindTaskFromProject(@NotNull String userId, @Nullable String projectId,
                                  @Nullable String taskId);

}