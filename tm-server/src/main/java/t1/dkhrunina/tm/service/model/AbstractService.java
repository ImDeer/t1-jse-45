package t1.dkhrunina.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.repository.model.IRepository;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.model.IService;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.field.IdEmptyException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected abstract R getRepository(@NotNull final EntityManager entityManager);

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        boolean result;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            result = repository.findOneById(id) != null;
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<M> findAll() {
        @Nullable final List<M> models;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            models = repository.findAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            model = repository.findOneById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize()) throw new IndexIncorrectException();
        @Nullable final M model;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            model = repository.findOneByIndex(index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public int getSize() {
        int result;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            result = repository.getSize();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        @Nullable final M removedModel;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            removedModel = repository.findOneById(model.getId());
            if (removedModel == null) throw new EntityNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(removedModel);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return removedModel;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M removedModel;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            removedModel = repository.findOneById(id);
            if (removedModel == null) throw new EntityNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(removedModel);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return removedModel;
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize()) throw new IndexIncorrectException();
        @Nullable final M removedModel;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            removedModel = repository.findOneByIndex(index);
            if (removedModel == null) throw new EntityNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(removedModel);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return removedModel;
    }

    @Override
    public void update(@NotNull final M model) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}