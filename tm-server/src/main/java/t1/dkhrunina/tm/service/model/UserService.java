package t1.dkhrunina.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.repository.model.IUserRepository;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.api.service.model.IProjectTaskService;
import t1.dkhrunina.tm.api.service.model.IUserService;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.exception.field.EmailEmptyException;
import t1.dkhrunina.tm.exception.field.IdEmptyException;
import t1.dkhrunina.tm.exception.field.LoginEmptyException;
import t1.dkhrunina.tm.exception.field.PasswordEmptyException;
import t1.dkhrunina.tm.exception.user.EmailExistsException;
import t1.dkhrunina.tm.exception.user.LoginExistsException;
import t1.dkhrunina.tm.exception.user.UserNotFoundException;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.repository.model.UserRepository;
import t1.dkhrunina.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;

public final class UserService extends AbstractService<User, IUserRepository>
        implements IUserService {

    @NotNull
    private final IProjectTaskService projectTaskService;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectTaskService projectTaskService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
        this.projectTaskService = projectTaskService;
    }

    @NotNull
    @Override
    protected IUserRepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    @NotNull
    @Override
    public User remove(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            projectTaskService.removeAllByUserId(user.getId());
            repository.remove(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        user.setEmail(email);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            user = repository.findByLogin(login);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User user;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            user = repository.findByEmail(email);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @NotNull
    @Override
    public Collection<User> set(@NotNull final Collection<User> users) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            for (@NotNull final User user : users)
                repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return users;
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        update(user);
        return user;
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

}