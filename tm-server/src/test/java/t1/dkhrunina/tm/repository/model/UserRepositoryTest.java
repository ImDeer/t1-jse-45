package t1.dkhrunina.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.api.repository.model.IUserRepository;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.service.ConnectionService;
import t1.dkhrunina.tm.service.PropertyService;
import t1.dkhrunina.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserRepositoryTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private static EntityManager entityManager;

    @NotNull
    private static IConnectionService connectionService;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initRepository() {
        userList = new ArrayList<>();
        entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        userRepository = new UserRepository(entityManager);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user" + i);
            user.setEmail("user" + i + "@email.ru");
            user.setFirstName("name " + i);
            user.setLastName("last name " + i);
            user.setPasswordHash(HashUtil.salt(new PropertyService(), "meow" + i));
            userRepository.add(user);
            userList.add(user);
        }
    }

    @After
    public void afterTest() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

    @Test
    public void addTest() {
        int expectedNumberOfEntries = userRepository.getSize() + 1;
        @NotNull final String userFirstName = "name";
        @NotNull final String userLastName = "last name";
        @NotNull final String userLogin = "login";
        @NotNull final String userEmail = "email@email.ru";
        @NotNull final User user = new User();
        user.setFirstName(userFirstName);
        user.setLastName(userLastName);
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        user.setPasswordHash("meow");
        userRepository.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        @Nullable final User createdUser = userRepository.findOneById(user.getId());
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(userFirstName, createdUser.getFirstName());
        Assert.assertEquals(userLastName, createdUser.getLastName());
        Assert.assertEquals(userLogin, createdUser.getLogin());
        Assert.assertEquals(userEmail, createdUser.getEmail());
        userRepository.remove(createdUser);
    }

    @Test
    public void addAllTest() {
        int expectedNumberOfEntries = userRepository.getSize() + 2;
        @NotNull final List<User> users = new ArrayList<>();
        @NotNull final String firstUserFirstName = "name 1";
        @NotNull final String firstUserLastName = "last name 1";
        @NotNull final String firstUserLogin = "login 1";
        @NotNull final String firstUserEmail = "email1@email.ru";
        @NotNull final User firstUser = new User();
        firstUser.setFirstName(firstUserFirstName);
        firstUser.setLastName(firstUserLastName);
        firstUser.setLogin(firstUserLogin);
        firstUser.setEmail(firstUserEmail);
        firstUser.setPasswordHash("meow");
        users.add(firstUser);
        @NotNull final String secondUserFirstName = "name 2";
        @NotNull final String secondUserLastName = "last name 2";
        @NotNull final String secondUserLogin = "login 2";
        @NotNull final String secondUserEmail = "email2@email.ru";
        @NotNull final User secondUser = new User();
        secondUser.setFirstName(secondUserFirstName);
        secondUser.setLastName(secondUserLastName);
        secondUser.setLogin(secondUserLogin);
        secondUser.setEmail(secondUserEmail);
        secondUser.setPasswordHash("meow");
        users.add(secondUser);
        @NotNull final Collection<User> addedUsers = userRepository.add(users);
        Assert.assertTrue(addedUsers.size() > 0);
        int actualNumberOfEntries = userRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
        userRepository.remove(firstUser);
        userRepository.remove(secondUser);
    }

    @Test
    public void testFindAll() {
        @Nullable final List<User> users = userRepository.findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
    }

    @Test
    public void testFindByLogin() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            @Nullable final User foundUser = userRepository.findByLogin(login);
            Assert.assertEquals(user, foundUser);
        }
    }

    @Test
    public void testFindByLoginNull() {
        @Nullable final User foundUserNull = userRepository.findByLogin(null);
        Assert.assertNull(foundUserNull);
        @Nullable final User foundUser = userRepository.findByLogin("meow");
        Assert.assertNull(foundUser);
    }

    @Test
    public void testFindByEmail() {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            @Nullable final User foundUser = userRepository.findByEmail(email);
            Assert.assertEquals(user, foundUser);
        }
    }

    @Test
    public void findByEmailNullTest() {
        @Nullable final User foundUserNull = userRepository.findByEmail(null);
        Assert.assertNull(foundUserNull);
        @Nullable final User foundUser = userRepository.findByEmail("meow");
        Assert.assertNull(foundUser);
    }

    @Test
    public void testFindOneById() {
        @Nullable User user;
        for (int i = 0; i < userList.size(); i++) {
            user = userList.get(i);
            Assert.assertNotNull(user);
            @NotNull final String userId = user.getId();
            @Nullable final User foundUser = userRepository.findOneById(userId);
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    public void testFindOneByIdNull() {
        @Nullable final User foundUser = userRepository.findOneById("meow");
        Assert.assertNull(foundUser);
        @Nullable final User foundUserNull = userRepository.findOneById(null);
        Assert.assertNull(foundUserNull);
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 1; i <= userList.size(); i++) {
            @Nullable final User user = userRepository.findOneByIndex(i);
            Assert.assertNotNull(user);
        }
    }

    @Test
    public void testFindOneByIndexNull() {
        @Nullable final User user = userRepository.findOneByIndex(null);
        Assert.assertNull(user);
    }

    @Test
    public void testGetSize() {
        int actualSize = userRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    public void testRemove() {
        @Nullable final User user = userList.get(1);
        Assert.assertNotNull(user);
        @NotNull final String userId = user.getId();
        @Nullable final User deletedUser = userRepository.remove(user);
        Assert.assertNotNull(deletedUser);
        @Nullable final User deletedUserInRepository = userRepository.findOneById(userId);
        Assert.assertNull(deletedUserInRepository);
    }

    @Test
    public void testRemoveNull() {
        @Nullable final User user = userRepository.remove(null);
        Assert.assertNull(user);
    }


}
